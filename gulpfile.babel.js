
import Taskerify from 'taskerify';

Taskerify.config.sourcemaps = false;
Taskerify.config.srcPath = './src/assets';
Taskerify.config.distPath = './dist/assets';
Taskerify.config.srcViewsPath = './src';
Taskerify.config.distViewsPath = './dist';

const NODE_MODULES = './node_modules';
const SRC = Taskerify.config.srcPath;
const DIST = Taskerify.config.distPath;

const storeName = 'teste-front';
const commonFiles = ['home', 'geral' ];

Taskerify((mix) => {

    // Javascript Linter
    mix.eslint();

    // Image Minifier
    mix.imagemin(`${SRC}/common/images`, `${DIST}/common/images`);

    // Common Files // Responsive
    commonFiles.map((file) => {
        mix.browserify(`${SRC}/common/js/${storeName}-${file}.js`, `${DIST}/common/js`)
            .sass(`${SRC}/common/scss/${storeName}-${file}.scss`, `${DIST}/common/css`);
    });


    // Mix Vendor Scripts
    mix.scripts([
    ], `${DIST}/common/js/${storeName}-vendor.js`);
});