## Instruções
Projeto criado na versão *6.10.2* do node.

*npm install* para instalar as dependencies ( em caso de erro, utilizar o comando *npm install --unsafe-perm* )

*npm run start* para rodar o projeto ( em caso de erro, utilizar o comando *sudo npm run start* )

Após essas instruções os arquivos de SASS e JS serão buildados para CSS3 e JS e o projeto estará rodando e pronto para ser utilizado.

## Especificações técnicas

- Utilizado no projeto HTML5, SASS(pré-processador de css3) e JQUERY (somente para a aplicação do slick).
- Projeto responsivo e Mobile First




